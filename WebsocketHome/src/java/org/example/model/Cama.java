/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.example.model;

/**
 *
 * @author Araceli
 * Definición del constructor, getter y setter
 */
public class Cama {
    private int id;
    private String nombre;
    private String estado;
    private String hospital;
    private String descripcion;

    public Cama() {
    }
    
    public int getId() {
        return id;
    }
    
    public String getNombre() {
        return nombre;
    }

    public String getEstado() {
        return estado;
    }

    public String getHospital() {
        return hospital;
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
