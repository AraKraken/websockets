/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.example.websocket;
import java.io.IOException;
import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonObject;
import javax.json.spi.JsonProvider;
import javax.websocket.Session;
import org.example.model.Cama;



/**
 *
 * @author Araceli
 */
@ApplicationScoped
public class CamaSessionHandler {
    
    private int camaId = 0;
    //se declara un hashSet para almacenar la lista de camas añadidas
    //a la aplicación y las sesiones activas en la aplicación
    
    //Cada cliente conectado a la aplicación tiene su propia sesión
    private final Set<Session> sessions = new HashSet<>();
    private final Set<Cama> camas = new HashSet<>();
    
    //metodos para agregar y remover sesiones
    public void addSession(Session session) {
        sessions.add(session);
        //envia la lista de todas las camas al cliente conectado
        for (Cama cama : camas) {
            JsonObject addMessage = createAddMessage(cama);
            sendToSession(session, addMessage);
        }
    }

    public void removeSession(Session session) {
        sessions.remove(session);
    }
    
    /*Se definen los metodos que operan en el objeto Cama
    addCama() - añadir una cama a la aplicación.
r   removeCama() - remover una cama a la aplicación.
t   alternarCama() - cambiar el estado de la cama.
g   getCamas() - Retorna la lista de todas las camas y sus atributos.
g   getCamaById() - Retorna una cama con un su id específico.
c   createAddMessage() - Crea un mensaje JSON para agregar una cama a la aplicación.
s   sendToSession() - Envia un mensaje de evento a un cliente
s   sendToAllConnectedSessions() - Envia un mensaje de evento a todos los clientes conectados
    
    */
    
    public List<Cama> getCamas() {
        return new ArrayList<>(camas);
    }

    public void crearCama(Cama cama) {
        cama.setId(camaId);
        camas.add(cama);
        camaId++;
        JsonObject addMessage = createAddMessage(cama);
        //envia un mensaje a todas las sesiones activas en el servidor
        sendToAllConnectedSessions(addMessage);
    }

    public void eliminarCama(int id) {
        Cama cama = getCamaById(id);
        if (cama != null) {
            camas.remove(cama);
            JsonProvider provider = JsonProvider.provider();
            JsonObject removeMessage = provider.createObjectBuilder()
                    .add("estado_operacion", 0)
                    .add("mensaje", "ok")
                    .add("tipo_operacion", "eliminar_cama")
                    .add("id", id)
                    .add("nombre", cama.getNombre())
                    .add("hospital", cama.getHospital())
                    .add("estado", cama.getEstado())
                    .add("descripcion", cama.getDescripcion())
                    .build();
            //envia un mensaje a todas las sesiones activas en el servidor
            sendToAllConnectedSessions(removeMessage);
        }
    }

    public void alternarCama(int id) {
        JsonProvider provider = JsonProvider.provider();
        Cama cama = getCamaById(id);
        if (cama != null) {
            if ("Ocupado".equals(cama.getEstado())) {
                cama.setEstado("Desocupado");
            } else {
                cama.setEstado("Ocupado");
            }
            JsonObject updateDevMessage = provider.createObjectBuilder()
                    .add("estado_operacion", 0)
                    .add("mensaje", "ok")
                    .add("tipo_operacion", "alternar_cama")
                    .add("id", cama.getId())
                    .add("nombre", cama.getNombre())
                    .add("hospital", cama.getHospital())
                    .add("estado", cama.getEstado())
                    .add("descripcion", cama.getDescripcion())
                    
                    
 /*                   .add("action", "toggle")
                    .add("id", cama.getId())
                    .add("estado", cama.getEstado())*/
                    .build();
            sendToAllConnectedSessions(updateDevMessage);
        }
    }

    
    //retorna la cama segun el id recibido
    private Cama getCamaById(int id) {
        for (Cama cama : camas) {
            if (cama.getId() == id) {
                return cama;
            }
        }
        return null;
    }

    private JsonObject createAddMessage(Cama cama) {
        JsonProvider provider = JsonProvider.provider();
        JsonObject addMessage = provider.createObjectBuilder()
                .add("estado_operacion", 0)
                .add("mensaje", "ok")
                .add("tipo_operacion", "crear_cama")
                .add("id", cama.getId())
                .add("nombre", cama.getNombre())
                .add("hospital", cama.getHospital())
                .add("estado", cama.getEstado())
                .add("descripcion", cama.getDescripcion())
                
                
/*                .add("action", "add")
                .add("id", cama.getId())
                .add("nombre", cama.getNombre())
                .add("hospital", cama.getHospital())
                .add("estado", cama.getEstado())
                .add("descripcion", cama.getDescripcion())*/
                .build();
        return addMessage;
    }

    private void sendToAllConnectedSessions(JsonObject message) {
        for (Session session : sessions) {
            sendToSession(session, message);
        }
    }

    private void sendToSession(Session session, JsonObject message) {
        try {
            session.getBasicRemote().sendText(message.toString());
        } catch (IOException ex) {
            sessions.remove(session);
            Logger.getLogger(CamaSessionHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
