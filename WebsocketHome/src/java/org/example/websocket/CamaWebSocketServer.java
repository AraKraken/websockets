/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.example.websocket;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.StringReader;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.example.model.Cama;
import java.util.logging.Level;
import java.util.logging.Logger;


@ApplicationScoped
@ServerEndpoint("/actions")
/**
 *
 * @author Araceli
 * Se define el endpoint de servidor
 */
public class CamaWebSocketServer {
    //se mapea las anotaciones del ciclo de vida de
    //websocket a los metodos de java
    //se crea el objeto cama tipo session para procesar los eventos en cada session
    @Inject
    private CamaSessionHandler sessionHandler;
    
    @OnOpen
        public void open(Session session) {
            sessionHandler.addSession(session);
    }

    @OnClose
        public void close(Session session) {
            sessionHandler.removeSession(session);
    }

    @OnError
        public void onError(Throwable error) {
            Logger.getLogger(CamaWebSocketServer.class.getName()).log(Level.SEVERE, null, error);
    }

        
    /*El metodo OnMessage hace las siguientes acciones
        -Lee las acciones de las camas y los atributos enviados desde el cliente.
        - Invoca al manejador de la session para manejar los operaciones apropiadas en el objeto
            Cama especificado. En esta aplicación, la acción de crear enviada desde el cliente
            invoca el metodo crearCama, la acción eliminar invoca eliminarCama, y la acción de alternar
            invoca alternarCama.
        */
    @OnMessage
        public void handleMessage(String message, Session session) {
            try (JsonReader reader = Json.createReader(new StringReader(message))) {
            JsonObject jsonMessage = reader.readObject();

            if ("crear_cama".equals(jsonMessage.getString("tipo_operacion"))) {
                Cama cama = new Cama();
                cama.setNombre(jsonMessage.getString("nombre"));
                cama.setDescripcion(jsonMessage.getString("descripcion"));
                cama.setHospital(jsonMessage.getString("hospital"));
                cama.setEstado("Desocupado");
                sessionHandler.crearCama(cama);
            }

            if ("eliminar_cama".equals(jsonMessage.getString("tipo_operacion"))) {
                int id = (int) jsonMessage.getInt("id");
                sessionHandler.eliminarCama(id);
            }

            if ("alternar_cama".equals(jsonMessage.getString("tipo_operacion"))) {
                int id = (int) jsonMessage.getInt("id");
                sessionHandler.alternarCama(id);
            }
        }
    }
}
