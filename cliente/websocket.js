/* 
 *En esta sección se especifica el endpoint del cliente usando javascripts
 */

/*
 *Se llevan a cabo las siguiente acciones para manejar los eventos en el cliente:
 * Mapear el endpoint del websocket server a la URI definida en la Creación del endpoint del servidor WebSocketcama
 * Capturar los eventos de Javascript para añadir, remover y cambiar el estado de una cama y pushear esos eventos en el servidor WebSocket.
 *      Estos métodos son crearCama(), eliminarCama(), and alternarCama(). Las acciones son enviadas en un mensaje en formato JSON al servidor WebSocket
 * Define un callback metodo para el evento onmessage. El onmessage evento captura los eventos enviados desde el servidor de WebSocket (en JSON) y procesa esas acciones.
 *  En esta aplicación,estas acciones suelen generar cambios en el UI del cliente.
 * Alterna la visibilidad de un formulario HTML para agregar una nueva cama.
 */

window.onload = init;
var socket = new WebSocket("ws://localhost:8080/WebsocketHome/actions");
socket.onmessage = onMessage;

function onMessage(event) {
    console.log(event.data);
    var cama = JSON.parse(event.data);
    if (cama.tipo_operacion === "crear_cama") {
        printCamaElement(cama);
    }
    if (cama.tipo_operacion === "eliminar_cama") {
        document.getElementById(cama.id).remove();
        //device.parentNode.removeChild(device);
    }
    if (cama.tipo_operacion === "alternar_cama") {
        var node = document.getElementById(cama.id);
        var statusText = node.children[2];
        if (cama.estado === "Ocupado") {
            statusText.innerHTML = "Estado de la cama: " + cama.estado + " (<a href=\"#\" OnClick=alternarCama(" + cama.id + ")>Desocupar Cama</a>)";
        } else if (cama.estado === "Desocupado") {
            statusText.innerHTML = "Estado de la cama: " + cama.estado + " (<a href=\"#\" OnClick=alternarCama(" + cama.id + ")>Ocupar Cama</a>)";
        }
    }
}

function crearCama(nombre, hospital, descripcion) {
    var CamaAction = {
        tipo_operacion: "crear_cama",
        nombre: nombre,
        hospital: hospital,
        descripcion: descripcion
    };
    socket.send(JSON.stringify(CamaAction));
}

function eliminarCama(element) {
    var id = element;
    var CamaAction = {
        tipo_operacion: "eliminar_cama",
        id: id
    };
    socket.send(JSON.stringify(CamaAction));
}

function alternarCama(element) {
    var id = element;
    var CamaAction = {
        tipo_operacion: "alternar_cama",
        id: id
    };
    socket.send(JSON.stringify(CamaAction));
}

function printCamaElement(cama) {
    var content = document.getElementById("content");
    
    var camaDiv = document.createElement("div");
    camaDiv.setAttribute("id", cama.id);
    camaDiv.setAttribute("class", "cama " + cama.hospital);
    content.appendChild(camaDiv);

    var camaName = document.createElement("span");
    camaName.setAttribute("class", "camaName");
    camaName.innerHTML = cama.nombre;
    camaDiv.appendChild(camaName);

    var camaHospital = document.createElement("span");
    camaHospital.innerHTML = "<b>Hospital:</b> " + cama.hospital;
    camaDiv.appendChild(camaHospital);

    var camaStatus = document.createElement("span");
    if (cama.estado === "Ocupado") {
        camaStatus.innerHTML = "<b>Estado:</b> " + cama.estado + " (<a href=\"#\" OnClick=alternarCama(" + cama.id + ")>Desocupar</a>)";
    } else if (cama.estado === "Desocupado") {
        camaStatus.innerHTML = "<b>Estado:</b> " + cama.estado + " (<a href=\"#\" OnClick=alternarCama(" + cama.id + ")>Ocupar</a>)";
        //deviceDiv.setAttribute("class", "device off");
    }
    camaDiv.appendChild(camaStatus);

    var camaDescription = document.createElement("span");
    camaDescription.innerHTML = "<b>Comentarios:</b> " + cama.descripcion;
    camaDiv.appendChild(camaDescription);

    var eliminarCama = document.createElement("span");
    eliminarCama.setAttribute("class", "eliminarCama");
    eliminarCama.innerHTML = "<a href=\"#\" OnClick=eliminarCama(" + cama.id + ")>Quitar Cama</a>";
    camaDiv.appendChild(eliminarCama);
}

function showForm() {
    document.getElementById("addCamaForm").style.display = '';
}

function hideForm() {
    document.getElementById("addCamaForm").style.display = "none";
}

function formSubmit() {
    var form = document.getElementById("addCamaForm");
    var name = form.elements["cama_name"].value;
    var hospital = form.elements["cama_hospital"].value;
    var descripcion = form.elements["cama_descripcion"].value;
    hideForm();
    document.getElementById("addCamaForm").reset();
    crearCama(name, hospital, descripcion);
}

function init() {
    hideForm();
}
