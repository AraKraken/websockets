**********WEBSOCKETS***********

*INTEGRANTES:
	-Derlis Ramos
	-Oscar Salinas
	-Araceli Silva
	-Nelson Ruiz
	
---------------------RESUMEN---------------------

	Las aplicaciones web modernas requieren más interactividad que nunca para las comunicaciones cliente / servidor. HTTP, sin embargo, no se creó para ofrecer el tipo de interactividad que se necesita en la actualidad. Las técnicas "Push" o Comet, como el sondeo prolongado, surgieron como una forma de permitir que un servidor envíe datos a un navegador. Debido a que estas técnicas generalmente se basan en HTTP, presentan algunas desventajas para las comunicaciones cliente / servidor, como la sobrecarga de HTTP. Estas desventajas dan como resultado una comunicación menos eficiente entre el servidor y el navegador web, especialmente para aplicaciones en tiempo real.

WebSocket proporciona una alternativa a esta limitación al proporcionar comunicaciones bidireccionales, full-duplex, en tiempo real, cliente / servidor. El servidor puede enviar datos al cliente en cualquier momento. Debido a que WebSocket se ejecuta sobre TCP, también reduce la sobrecarga de cada mensaje. WebSocket también proporciona una mayor escalabilidad para aplicaciones con uso intensivo de mensajes porque solo se usa una conexión por cliente (mientras que HTTP crea una solicitud por mensaje). Finalmente, WebSocket es parte de Java EE 8, por lo que puede usar otras tecnologías en la pila de Java EE 8.

*APLICACIÓN WEB JAVA EE 8 CON WEBSOCKETS

Introducido como parte de la iniciativa HTML 5, el protocolo WebSocket es una tecnología web estándar que simplifica la gestión de la comunicación y la conexión entre los clientes y un servidor. Al mantener una conexión constante, WebSocket proporciona comunicación cliente / servidor full-duplex. También proporciona una comunicación de bajo nivel y baja latencia que funciona en la conexión TCP / IP subyacente.

La API de Java para WebSocket ( JSR-356 ) simplifica la integración de WebSocket en las aplicaciones Java EE 8.

Estas son algunas de las características de la API de Java para WebSocket:

	-Programación basada en anotaciones que permite a los desarrolladores usar POJO (Plain Old Java Object) es simplemente un objeto de Java que no implementa ninguna interfase especiaL,
para interactuar con eventos del ciclo de vida de WebSocket.
	-Programación basada en interfaces que permite a los desarrolladores implementar interfaces y métodos para interactuar con los eventos del ciclo de vida de WebSocket.
	-Integración con otras tecnologías Java EE (puede inyectar objetos y Enterprise JavaBeans utilizando componentes como Contexts e Inyección de dependencias).

*REQUERIMIENTOS DE SOFTWARE:

	-Descargar e instalar el kit de desarrollo de software (JDK8) de Java EE desde: 
		 https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html
		 
	-Descargar e instalar el entorno de desarrollo integrado (IDE) Java NetBeans 12 		desde: 
		http://www.netbeans.org/downloads/index.html 
	-Descargar e instalar Oracle GlassFish Server 5.0 desde 
		https://www.oracle.com/java/technologies/javaee-8-sdk-downloads.html
		
	-Configurar el servidor de Netbeans IDE con Glassfish.
		En Apache Netbeans vaya a la opción <tools> luego seleccione <servers> y añada 			glassfish5 según la ruta de su directorio en su ordenador. Luego guarde los 			cambios seleccionando el botón <ok>.
	
	*****EJECUTAR COMPONENTES****
	-
	-Ejecutar proyecto en el IDE (click en botón <run>). Esto activa el servidor y abre la 	conexión.
	-Abrir index.html desde el navegador (clientes).
	-Abrir consola del navegador para visualizar los mensajes json entre cliente y 		servidor.
	-Obs: ejecutar proyecto "websockethome" del repositorio git.

*API DE SERVICIOS 

	https://javaee.github.io/glassfish/documentation
	
	API web>websockets
		https://developer.mozilla.org/es/docs/Web/API/WebSockets_API 		Writing_WebSocket_client_applications		
	
	**WebSocket.onmessage**
	La WebSocket.onmessagepropiedad es un controlador de eventos al que se llama cuando se 		recibe un mensaje del servidor. Se llama con MessageEvent.
	
	Sintaxis:
		aWebSocket.onmessage = function(event) {
  			console.debug("WebSocket message received:", event);
		};
	

